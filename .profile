# Sample .profile for SuSE Linux
# rewritten by Christian Steinruecken <cstein@suse.de>
#
# This file is read each time a login shell is started.
# All other interactive shells will only read .bashrc; this is particularly
# important for language settings, see below.

test -z "$PROFILEREAD" && . /etc/profile || true

source $HOME/.ctl-local-hostname 2> /dev/null
if [ -z ${CTL_LOCAL_HOSTNAME+x} ]; then echo 'Warning: $CTL_LOCAL_HOSTNAME is not set! (.profile)'; fi

MY_PATH=$HOME/wd/app/bin
MY_PATH=$HOME/ctl/x:$MY_PATH
MY_PATH=$HOME/ctl/x.$CTL_LOCAL_HOSTNAME:$MY_PATH
MY_PATH=$HOME/.local/bin:$MY_PATH

export PATH=$MY_PATH:$PATH
export CTL_ZSH_THEME=agnoster

eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)

# Most applications support several languages for their output.
# To make use of this feature, simply uncomment one of the lines below or
# add your own one (see /usr/share/locale/locale.alias for more codes)
# This overwrites the system default set in /etc/sysconfig/language
# in the variable RC_LANG.
#
#export LANG=de_DE.UTF-8	# uncomment this line for German output
#export LANG=fr_FR.UTF-8	# uncomment this line for French output
#export LANG=es_ES.UTF-8	# uncomment this line for Spanish output
#export LC_TIME="en_GB.UTF-8"

# Some people don't like fortune. If you uncomment the following lines,
# you will have a fortune each time you log in ;-)

if [ -x /usr/bin/fortune ] ; then
    echo
    /usr/bin/fortune
    echo
fi

# Go directly to man(1) when calling man
export MAN_POSIXLY_CORRECT=1

setxkbmap -option caps:escape

CTL_LOCAL_PROFILE=~/ctl/profile.$CTL_LOCAL_HOSTNAME
if [ -f $CTL_LOCAL_PROFILE ]; then
    source $CTL_LOCAL_PROFILE
else
    print "Warning: $CTL_LOCAL_PROFILE not found."
fi
