echo "=== Turning PROXY off ==="

unset HTTPS_PROXY
unset HTTP_PROXY
unset FTP_PROXY
unset https_proxy
unset http_proxy
unset ftp_proxy
unset NO_PROXY
unset no_proxy

echo 'HTTPS_PROXY='$HTTPS_PROXY
echo 'HTTP_PROXY='$HTTP_PROXY
echo 'FTP_PROXY='$FTP_PROXY
echo 'https_proxy='$https_proxy
echo 'http_proxy='$http_proxy
echo 'ftp_proxy='$ftp_proxy
echo 'NO_PROXY='$NO_PROXY
echo 'no_proxy='$no_proxy

echo "========================="
