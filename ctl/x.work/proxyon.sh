echo "================== Turning PROXY on =================="

export HTTPS_PROXY=$CTL_SAP_PROXY_URL
export HTTP_PROXY=$CTL_SAP_PROXY_URL
export FTP_PROXY=$CTL_SAP_PROXY_URL
export https_proxy=$CTL_SAP_PROXY_URL
export http_proxy=$CTL_SAP_PROXY_URL
export ftp_proxy=$CTL_SAP_PROXY_URL
export NO_PROXY=$CTL_SAP_NO_PROXY
export no_proxy=$CTL_SAP_NO_PROXY

echo 'HTTPS_PROXY='$HTTPS_PROXY
echo 'HTTP_PROXY='$HTTP_PROXY
echo 'FTP_PROXY='$FTP_PROXY
echo 'https_proxy='$https_proxy
echo 'http_proxy='$http_proxy
echo 'ftp_proxy='$ftp_proxy
echo 'NO_PROXY='$NO_PROXY
echo 'no_proxy='$no_proxy

echo "======================================================"
