setxkbmap -option caps:escape

cmatrix
pipes.sh
acsiiaquarium
sl
gnomesu

mcrypt
gpg
du -hs
nethogs
readelf
/usr/share/icons/Adwaita


xlock -mode matrix
light-locker
/usr/lib/gdmflexiserver

for filename in *.srt; do iconv -f Windows-1250 -t UTF-8 $filename > utf/$filename; done

sudo mount -t exfat /dev/sde1 /media/usb
sudo umount /dev/sde1

shred -zvu -n  5 passwords.list

fdisk
poweroff
lsblk

unetbootin

udisksctl
- status
- mount -b
- unmount-b /dev/sdd1
- powe-off -b /dev/sdd

LVM Management
- lvdisplay
- pvs
- vgscan

# Groups management
Add a user to a secondary group
- sudo usermod -a -G vboxusers,anothergroup,thirdgroup pyty 
Remove user from a group
- sudo gpasswd -d username groupname
Change users primary group
- sudo usermod -g groupname username

# RPM Changelog
rpm -q --changelog kernel-default | less

# Sysinfo
inxi
hwinfo

# Check for bad blocks
Destructive writemode, show progress, verbose output
sudo badblocks -vsw /dev/sda > ~proj/predaj-diskov/badsectors-wd-blue-1tb-2.txt   

# Read enviroment of a process
`xargs -n 1 -0 < /proc/<pid>/environ`

# Allow user to run a command without entering password
Add to `/etc/sudoers`
`pyty  ALL = NOPASSWD: /sbin/shutdown`
then `sudo shutdown -r now`

# Run bash and start zsh immediately
`bash --init-file <(echo "zsh")`

# Search for depndencies
`zypper se --provides file_name`
`cnf file_name`

# Install RPM (ignore missing deps)
`sudo rpm -Uhv <pkg-file> --nodeps`

