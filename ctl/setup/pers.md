# Static IPs

|||
|-:|:-|
|   Cisco Router: | 192.168.100.1 |
| TP-Link Router: | 192.168.1.1   |
|            NAS: | 192.168.1.2   |
|||
| Workstation PC: | 192.168.1.10  |

# DHCP range

192.168.1.100 - 192.168.1.199

# DNS servers (Google) - set in TP-Link router

8.8.8.8
8.8.4.4
