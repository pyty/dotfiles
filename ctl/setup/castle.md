# Prepare home directory
Note: for this procedure nothing needs to be set in /etc, exept XKBOptions

1. export CTL_LOCAL_HOSTNAME in ~/.ctl-local-hostname file which is soruced in ~/.profile
2. set XKBOptions in `/etc/X11/xorg.conf.d/00-keyboard.conf` (this is also set in ~/.profile)
3. install homebrew https://docs.brew.sh/Homebrew-on-Linux
4. install yadm - `brew install yadm`
5. clone repo - `yadm clone https://gitlab.com/pyty/dotfiles.git`

# Bootstraping

1. System-wide actions
  - Create ~/.ctl-local-hostname file which exports CTL_LOCAL_HOSTNAME="pers"
  - System-wide XKBOptions `/etc/X11/xorg.conf.d/00-keyboard.conf` (this is in `~/.profile` as well)
  - PATHS are exported in `~/.profile`, only 
    - ~wd/app/bin
    - ~ctl/x.$CTL_LOCAL_HOSTNAME
  - Additional PAHT without restars can be set in .zshrc without export eg PATH=[new-paht]:$PATH

2. Local actions
   - Local actions not handled by LOCAL_CTL
       - eg. `.gitconfig` (automate link creation with a setup script)
   - Setup `oh-my-zsh` (try `prezto` or `grml's zsh config`)
   - Setup terminal emulator XFCE-termnal (try `termite` or `urxvt` or `terminator`)
   - Setup dropdown terminal Guake (try `tilda`)
   - Setup locals, keyboard layouts, keybindings
   - setup windowsmanager, shortcuts, virtual desktops
   - Setup packages (Ansible?, linuxbrew)
   - Setup fonts
   - essential cli utils - fzf etc.
